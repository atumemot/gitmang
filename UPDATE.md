# GITMANG UPDATE LOG

Most recent version is listed first.

### v1.3.0 March 28, 2022 9:40PM PDT

+ Added the [-p preset] command line option.
    + Presets are a way to quickly set up gitmang with options that are regularly used.
    + If the preset name is already defined, then gitmang will begin running using that preset. If the preset name is new, then a new preset will be created. If no preset name is specified, then the list of saved presets will be displayed.
+ Added the [-x preset] command line option to delete to specified preset.
+ Added the presets.gm file, which contains all created presets. This file can be editted directly.
+ Added the gm_usage.sh script, which contains all usage messages for gitmang programs.
+ Minor documentation revisions.

### v1.2.1 March 24, 2022 6:50 PDT

+ Commit count is now correct in program output.
+ *.[file] now behaves properly in the [r file] runtime option.
+ Using a runtime option no longer causes another commit cycle to run.
+ Added the [c] runtime option to immediately perform a commit.
+ Added the [t] runtime option to display the time remaining until the next commit.
+ Removed runtime options from the usage and synopsis displayed with [-h]. They can still be viewed using the [h] runtime option.
+ The [f freq] runtime option now displays the current interval if no freq is specified.
+ Minor documentation revisions.

### v1.2.0 March 22, 2022 3:30PM PDT

+ "Input Options" are now referred to as "Runtime Options" to prevent confusion with "Command Line Options".
+ Other small documentation revisions.
+ Added branch switching with the [-b branch] option. The same can be done with the [b branch] runtime option to switch branches during runtime.
+ Verbose output can now be enabled with the [-v] option. Using the [v] runtime option toggles verbose output during runtime.
+ Commit and push summaries can now be silenced with [-s] option. Using the [s] runtime option toggles the summaries during runtime.
+ Added the [l] runtime option, which displays what files are currently in the commit list.
+ Added the [r file] runtime option, which removes the file name from the commit list.

### v1.1.2 March 18, 2022 7:30PM PDT

+ The total time is now correctly output when the program ends.
+ Small improvements to formatting.
+ Added bug reporting form, link can be found in the PROBLEMS section of the README.

### v1.1.1 March 17, 2022 7:30PM PDT

+ Improved directory navigation of the [-dDR dir] commands. They now have more predictable behavior, and are more flexible with various directory structures.
+ Improved error handling for invalid directory inputs.
+ *.c and *.h are no longer included in the commit list by default.
+ Revised documentation.
+ Added this update log.