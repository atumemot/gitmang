#!/bin/bash

# USAGE STATEMENTS -------------------------------------------------------

gm_rto=$(./.gm_usage.sh -r) # runtime options

# COMMAND LINE ARGUMENTS -------------------------------------------------

# all vars are prepended with "gm_" to avoid overriding other global vars

# default options
gm_verb=0
gm_sil=""
gm_dir=""
gm_branch="master"
gm_main=0
gm_freq=5
gm_reps=100
gm_repair=0
gm_backup=0
gm_files=()

# process command line arguments
while getopts vsp:x:d:D:b:f:r:R:Bh opt; do
	case "${opt}" in
		v) gm_verb=1;; # verbose output

		s) gm_sil="-q";; # silence summaries

		p) # presets
			if [[ ${OPTARG} == "ALL" ]]; then # display all presets
				echo "Available presets:"
				while read -r line; do
					echo "$line"
				done < presets.gm
				exit
			else
				# look for preset
				while read -r -a gm_preset; do
					# if preset is found
					if [[ "${gm_preset[0]}" == "${OPTARG}" ]]; then
						echo "Preset ${gm_preset[*]}"

						# parsing preset options
						for (( gm_i=1; $gm_i < ${#gm_preset[@]}; gm_i+=1)); do
							case "${gm_preset[$gm_i]}" in
								"-v") gm_verb=1;;
								"-s") gm_sil=1;;
								"-d") 
									gm_dir=${gm_preset[$((gm_i+1))]}
									((gm_i+=1));;
								"-D") 
									gm_dir=${gm_preset[$((gm_i+1))]}
									gm_main=1
									((gm_i+=1));;
								"-b") 
									gm_branch=${gm_preset[$((gm_i+1))]}
									((gm_i+=1));;
								"-f") 
									gm_freq=${gm_preset[$((gm_i+1))]}
									((gm_i+=1));;
								"-r") 
									gm_reps=${gm_preset[$((gm_i+1))]}
									((gm_i+=1));;
								*) gm_files+=( "${gm_preset[$gm_i]}" );;
							esac
						done
					fi
				done < presets.gm

				# create new preset, dir must be specified so if it is not
				# then no preset was found
				if [[ $gm_dir == "" ]]; then
					./.gm_usage.sh -c
					echo ""
					echo "Creating new preset ${OPTARG}."
					read -p "Preset commands: " gm_preset
					echo "${OPTARG} $gm_preset" >> presets.gm
					echo "Preset ${OPTARG} created, use preset with ./gitmang.sh -p ${OPTARG}."
					exit
				fi
			fi;;
		
		x) # delete preset
			if [[ ${OPTARG} == "" ]]; then
				echo "A preset must be specified to delete."
			else
				# look for preset
				gm_i=0
				while read -r -a gm_preset; do
					if [[ "${gm_preset[0]}" != "${OPTARG}" ]]; then
						echo "${gm_preset[*]}" >> .preset_temp.gm
						((gm_i+=1))
					fi
				done < presets.gm

				# creates temp file in case there is only one preset
				if (( $gm_i == 0 )); then
					> .preset_temp.gm
				fi

				# replaces old presets file
				mv .preset_temp.gm presets.gm
				echo "Preset ${OPTARG} has been deleted."
			fi
			
			exit;;

		d) gm_dir=${OPTARG};; # repo directory from working dir

		D) # repo directory from main dir
			gm_dir=${OPTARG}
			gm_main=1;;

		b) # branch name
			gm_branch=${OPTARG};;

		f) # frequency
			if (( ${OPTARG} > 0 )); then
				gm_freq=${OPTARG}
			else
				echo "Interval must be greater than 0, will remain 5 mins."
			fi;;

		r) # reps
			if (( ${OPTARG} > 0 )); then
				gm_reps=${OPTARG}
			else
				echo "Number of commits must be greater than 0, will remain 100."
			fi;;

		R) # repair directory
			gm_dir=${OPTARG}
			gm_repair=1;;

		B) gm_backup=1;; # keep backup

		h) # program usage and synopsis
			./.gm_usage.sh -h
			exit;;
		*)
			./.gm_usage.sh -h
			exit;;
	esac
done

# test for no directory input
if [[ $gm_dir == "" ]]; then
	echo "Repo directory must be specified, exitting program."
	exit	
fi

# repair repo
if (( $gm_repair == 1 )); then
	# get gitmang dir for error testing
	gm_gmdir=$(pwd)

	# try to move into dir
	cd $gm_dir

	# test if gm_dir is a valid path
	if [[ $gm_gmdir == $(pwd) ]]; then
		echo "$gm_dir is an invalid directory, exitting program."
		exit
	fi

	# get remote repo url
	gm_url=$(git config --get remote.origin.url)

	# test if gm_dir is a valid repo
	if [[ $gm_url == "" ]]; then
		echo "$gm_dir is not a repository or the remote repository URL is corrupt, exitting program."
		exit
	fi

	gm_repo=$(basename $(pwd))
	cd ../

	# make temp dir
	mkdir gm_repair_backup

	# copy files into temp dir
	cp -r $gm_repo/* gm_repair_backup

	echo "Repair backup created."

	# delete dir and re clone
	rm -rf $gm_repo
	git clone $gm_url $gm_repo

	# replace files
	rm -rf $gm_repo/*
	cp -r gm_repair_backup/* $gm_repo

	# delete temp dir
	if (( $gm_backup == 0 )); then
		echo "Deleting repair backup."
		rm -rf gm_repair_backup
	fi

	echo "Repository repair complete."

	exit
fi

# appends all files specified on command line to files array
shift $(($OPTIND - 1))
gm_files+=( $@ )

# moves into specified directory
if (( gm_main == 1 )); then
	cd
fi
cd $gm_dir

# test if gm_dir is a valid path
if [[ $(basename $gm_dir) != $(basename $(pwd)) ]]; then
	echo "Failed to move into $gm_dir, exitting program."
	exit
fi

# test if gm_dir is a valid repo
if [[ $(git config --get remote.origin.url) == "" ]]; then
	echo "$gm_dir is not a repository, exitting program."
	exit
fi

# move into branch
if [[ $gm_branch == "" ]]; then
	echo "A branch name must be specified, remaining in master branch."
	gm_branch="master"
fi

# create branch if it does not exist
if [[ $(git rev-parse --quiet --verify $gm_branch) == "" ]]; then
	git branch $gm_branch
	git checkout $gm_branch
	git push $gm_sil --set-upstream origin $gm_branch
	echo "Created new branch $gm_branch."
else
	git checkout $gm_branch
fi

echo "Moved to $gm_branch branch."
git branch

# PROGRAM RUNTIME LOOP ---------------------------------------------------

gm_sreps=0 # counts how many reps were completed while program was running
gm_cmd=""  # stores command if one is used during program runtime
gm_arg=""  # stores argument if command requires one

# to track when the script started, used for output
gm_stime=$(date "+%s") # UNIX timestamp

# commit loop
while (( $gm_reps > 0 )); do

	# commit steps
	trap "" SIGINT
	git pull
	for gm_ft in "${gm_files[@]}"; do
		git add $gm_ft
	done
	gm_com=$(git commit $gm_sil -m "autosaving progress $(date)")
	echo "$gm_com"
	git push $gm_sil
	trap - SIGINT
	
	# tracking progress
	((gm_reps-=1))
	if [[ $(echo "$gm_com" | grep -o "nothing to commit") != "nothing to commit" ]]; then
		((gm_sreps+=1))
	fi
	
	if (( $gm_reps > 0 )); then
		# nice formatting
		echo "------------------------------------------------------------"

		# verbose output
		if (( $gm_verb == 1 )); then
			echo "$gm_sreps commit(s) made in $(basename $(pwd)) on $gm_branch branch."
			echo "Current program runtime: $(( $(( $(date "+%s") - $gm_stime )) / 60 )) min(s)."
			echo ""
		fi

		gm_ctime=$(date "+%s") # used to track interval start time
		gm_ftime=$(( $(( $(date "+%s") - $gm_ctime )) / 60 )) # time left in interval
		gm_cont=0 # set to 1 to exit the runtime command input loop

		# program runtime options
		while (( $gm_ftime < $gm_freq && $gm_cont == 0 )); do
			read -t $(($(($gm_freq - $gm_ftime)) * 60)) -p "Input runtime command, use h for options: " gm_cmd gm_arg
			case "$gm_cmd" in
				q) # quit
					gm_reps=1
					gm_cont=1;;

				c) # immediately perform a commit
					gm_cont=1;;

				t) # display remaining time in interval
					echo "$(($gm_freq - $gm_ftime)) mins left before next commit.";;

				v) # toggle verbose output
					if (( $gm_verb == 0 )); then
						gm_verb=1
						echo "Enabled verbose program output."
					else
						gm_verb=0
						echo "Disabled verbose program output."
					fi;;

				s) # toggle summary silence
					if [[ $gm_sil == "-q" ]]; then
						gm_sil=""
						echo "Unsilenced commit and push summaries."
					else
						gm_sil="-q"
						echo "Silenced commit and push summaries."
					fi;;

				d)  # changed directories
					# check for invalid directory
					if [[ gm_arg == "" ]]; then
						echo "Repo directory must be specified."
					else
						gm_tmpdir=$(pwd)
						cd $gm_arg

						# test if gm_dir is a valid path
						if [[ $gm_tmpdir == $(pwd) ]]; then
							echo "Failed to move into $gm_arg, remaining in $gm_dir."
						else
							# test if gm_dir is a valid repo
							if [[ $(git config --get remote.origin.url) == "" ]]; then
								echo "$gm_arg is not a repository, remaining in $gm_dir."
								cd
								cd $gm_tmpdir
							else
								gm_dir=$gm_arg
								echo "Switched directory to $gm_arg."
							fi
						fi
					fi;;
			
				b) # change branch
					if [[ $gm_arg == "" ]]; then
						git branch
					else
						gm_branch=$gm_arg

						# create branch if ti does not exist
						if [[ $(git rev-parse --quiet --verify $gm_branch) == "" ]]; then
							git branch $gm_branch
							git checkout $gm_branch
							git push $gm_sil --set-upstream origin $gm_branch
							echo "Created new branch $gm_branch."
						else
							git checkout $gm_branch
						fi

						echo "Moved to $gm_branch branch."
						git branch
					fi;;

				f) # change interval
					if [[ $gm_arg == "" ]]; then
						echo "Interval between commits is currently $gm_freq mins."
					else
						if (( $gm_arg > 0 )); then
							gm_freq=$gm_arg
							echo "Changed interval to $gm_arg minute(s)."
						else
							echo "Interval must be greater than 0, remaining at $gm_freq mins."
						fi
					fi;;

				a) # add file
					if [[ $gm_arg == "" ]]; then
						echo "A file name must be specified."
					else
						gm_files+=( "$gm_arg" )
						echo "Added $gm_arg to the commit list."
					fi;;

				r) # remove file
					if [[ $gm_arg == "" ]]; then
						echo "A file name must be specified."
					else
						gm_i=0 # iterator for commit list

						if [[ $gm_arg == "*" ]]; then # removes all files
							gm_files=()
							echo "Removed all file names from the commit list."
						elif [[ $(echo "$gm_arg" | grep -o "\*\.[a-z]*") != "" ]]; then # test for *.file
							gm_arg=$(echo "$gm_arg" | grep -o "\.[a-z]*")

							# search through commit list for file extension
							for gm_fname in "${gm_files[@]}"; do
								if [[ $(echo "$gm_fname" | grep "$gm_arg") != "" ]]; then
									unset "gm_files[$gm_i]"
									echo "Removed $gm_fname from the commit list."
								fi

								((gm_i+=1))
							done
						else
							# search through commit list for exact file name
							for gm_fname in "${gm_files[@]}"; do
								if [[ $gm_fname == $gm_arg ]]; then
									unset "gm_files[$gm_i]"
									echo "Removed $gm_fname from the commit list."
								else
									((gm_i+=1))
								fi
							done

							if (( $gm_i > ${#gm_files[@]} )); then
								echo "$gm_arg was not found in the commit list."
							fi
						fi
					fi;;

				l) # print commit list
					echo "Current file names in the commit list:"
					echo "${gm_files[*]}";;

				h) echo "$gm_rto";; # runtime options

				*) echo "Failed to execute runtime command.";;
			esac

			gm_ftime=$(( $(( $(date "+%s") - $gm_ctime )) / 60 )) # time left in interval
		done
	fi

	# nice formatting
	echo ""
	echo "------------------------------------------------------------"

done

# OUTPUT AND PROGRAM END -------------------------------------------------

# program output
gm_total_t=$(( $(( $(date "+%s") - $gm_stime )) / 60 )) # minutes since loop started

echo "$gm_sreps commit(s) completed over $gm_total_t minute(s)."