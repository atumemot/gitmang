#!/bin/bash

function gm_rto {
	echo "RUNTIME OPTIONS"
	echo "    q        Quits the program."
	echo "    c        Immediately perform a commit."
	echo "    t        Display the time remaining until the next commit."
	echo "    v        Toggle verbose program output."
	echo "    s        Toggle commit and push summaries."
	echo "    d dir    Changes the git repository being managed."
	echo "               The file path should be from the current working directory."
	echo "    b branch Moves to branch. Creates branch if it does not exist."
	echo "               If no branch is specified, then the branch list will be printed."
	echo "    f freq   Changes the interval between each commit."
	echo "               If no freq is specified, then the current interval will be displayed."
	echo "    a file   Add a file name to the commit list."
	echo "    r file   Remove a file name from the commit list."
	echo "    l        Display the commit list."
	echo "    h        Display program runtime options."
}

function gm_clo {
    echo "COMMAND LINE OPTIONS"
	echo "    -v        Display verbose program output."
	echo "    -s        Silence commit and push summaries."
	echo "    -p preset Use preset gitmang options."
    echo "    -x preset Delete the specified preset."
	echo "    -d dir    The file path, from the current working directory, to the repository being managed."
	echo "    -D dir    The file path, from the main directory, to the repository being managed."
	echo "    -b branch Begins gitmang in branch. Creates branch if it does not exist (default: master)."
	echo "    -f freq   The time interval in minutes between each commit (default: 5)."
	echo "    -r reps   The number of commits to be made before ending (default: 100)."
	echo "    -R dir    Repairs the corrupted repository dir, saving un-committed files."
	echo "                Path is from working directory."
	echo "    -B        Keeps gm_repair_backup directory after repository repair."
	echo "                This can be used as a backup in case [-R dir] fails."
	echo "    file1 ... File names that will be added to the commit list."
	echo "    -h        Display program usage and synopsis."
}

function gm_usage {
	echo "GITMANG.SH SYNOPSIS: For more usage information, view the README."
	echo "    A shell script that commits specified files at a regular interval."
	echo ""
	echo "USAGE"
	echo "    $ ./gitmang.sh [-vsBh] [-px preset] [-dDR dir] [-b branch] [-f freq] [-r reps] [file1 file2 ...]"
	echo ""
	gm_clo
	echo ""
	echo "OUTPUT"
	echo "    Prints to stdout: <reps> commit(s) completed over <total time> minute(s)"
	echo ""
	echo "AUTHOR"
	echo "    Anthony Umemoto 2022, with debugging assistance from Omar Ahmadyar."
}

function gm_full {
    echo "GITMANG.SH SYNOPSIS: For more usage information, view the README."
	echo "    A shell script that commits specified files at a regular interval."
	echo ""
	echo "USAGE"
	echo "    $ ./gitmang.sh [-vsBh] [-px preset] [-dDR dir] [-b branch] [-f freq] [-r reps] [file1 file2 ...]"
	echo ""
	gm_clo
    echo ""
    gm_rto
	echo ""
	echo "OUTPUT"
	echo "    Prints to stdout: <reps> commit(s) completed over <total time> minute(s)"
	echo ""
	echo "AUTHOR"
	echo "    Anthony Umemoto 2022, with debugging assistance from Omar Ahmadyar."
}

# process command line arguments
while getopts hcr opt; do
	case "${opt}" in
        h)
            gm_usage
            exit;;
		c)
			gm_clo
			exit;;
        r)
            gm_rto
            exit;;
        *)
            gm_full
            exit;;
    esac
done