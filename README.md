# GITMANG v1.3.0

gitmang can be downloaded off the gitLab web interface, or the gitmang master branch can be pulled using:

`$ git clone git@git.ucsc.edu:atumemot/gitmang gitmang`

gitmang is an ongoing project, regularly check this repository for bug fixes and improvements. Refer to UPDATE.md for changes between versions.

A second branch called beta is used for testing upcoming versions. It is not recommended to use the gitmang from this branch, as it may be unstable and incomplete.

## DESCRIPTION

A shell script for automated git repository management in the command line environment. It commits specified files at a regular interval. Each commit will have the comment `autosaving progress <timestamp>`.

## PROGRAM FILES

+ gitmang.sh: The shell script for automating git repository management.
+ .gm_usage.sh: Contains usage statements for gitmang.
+ presets.gm: Contains created presets.
+ UPDATE.md: Lists each version of gitmang, and what changes were made.
+ README.md: Explains how to use gitmang.

## USAGE

`$ ./gitmang.sh [-vsBh] [-px preset] [-dDR dir] [-b branch] [-f freq] [-r reps] [file1 file2 ...]`

git must be installed, and gitmang should be run in a UNIX or git bash terminal. gitmang is currently untested in other terminals such as the Windows command prompt, and the macOS terminal. To give executing permissions use:

`$ chmod +x gitmang.sh` and `$ chmod +x .gm_usage.sh`

gitmang requires that a terminal window is dedicated to running it.

When quitting the program, use the 'q' runtime option to quit properly. Always remember to quit properly before turning off your machine. If gitmang is unexpectedly ended while it is performing a commit, the local repository may become corrupt. If this happens, use the [-R dir] command to repair the local repository to save un-committed work. However, this command only works if the remote repository URL is uncorrupted. To test if it is, use:

`$ git config --get remote.origin.url`

If the correct URL is returned, then the repair should work.

To make gitmang a more convenient tool to use, presets can be made with the [-p preset] command line option, so that it doesn't require you to repeatedly type the same commands for a project repository you work in often. For example:

`13s_asgn4 -v -s -d ../cse13s/asgn4 -f 10 *.c *.h Makefile README.md`

is a preset that can be made for a cse13s class' assignment 4. This is how the preset will appear in the presets.gm file (preset name first, followed by its options), which contains all created presets and can be editted directly. While presets can be added by typing them into the presets.gm file by hand, it is recommended that the command line option is used instead. This preset can then be used with the same [-p preset] option used to create it:

`$ ./gitmang.sh -p 13s_asgn4`

And can be deleted with the [-x preset] command line option:

`$ ./gitmang.sh -x 13s_asgn4`

## COMMAND LINE OPTIONS

### GITMANG.SH

`$ ./gitmang.sh [-vsBh] [-px preset] [-dDR dir] [-b branch] [-f freq] [-r reps] [file1 file2 ...]`

`-v` Display verbose program output.

`-s` Silence commit and push summaries.

`-p preset` Use preset gitmang options. Create a new preset if the name does not exist. Display all presets if `ALL` is specified.

`-x preset` Delete the specified preset.

`-d dir` The file path, from the current working directory, to the repository being managed.

`-D dir` The file path, from the main directory, to the repository being managed (recommended for UNIX only).

`-b branch` Begins gitmang in branch. Creates branch if it does not exist (default: master).

`-f freq` The time interval in minutes between each commit (default: 5).

`-r reps` The number of commits to be made before ending (defualt: 100).

`-R dir` Repairs the corrupted repository dir (path from working directory), saving un-committed files.

`-B` Keeps gm_repair_backup directory after repository repair. This can be used as a backup in case [-R dir] fails. gm_repair_backup is placed in the same directory the repaired repository is a child of.

`file1 file2 ...` File names that will be added to the commit list.

`-h` Display program usage and synopsis.

### .GM_USAGE.SH

`./.gm_usage.sh [-hcr]`

`-h` Display gitmang usage and synopsis.

`-c` Only display gitmang command line options.

`-r` Only display gitmang runtime options.

## RUNTIME OPTIONS

`q` Quits the program.

`c` Immediately perform a commit.

`t` Display the time remaining until the next commit.

`v` Toggle verbose program output.

`s` Toggle commit and push summaries.

`d dir` Changes the git repo being managed. The file path should be from the current working directory.

`b branch` Moves to branch. Creates branch if it does not exist. If no branch is specified, then the branch list will be printed.

`f freq` Changes the interval between each commit. If no freq is specified, then the current interval will be displayed.

`a file` Add a file name to the commit list.

`r file` Remove a file name from the commit list.

`l` Display the commit list.

`h` Display program runtime options.

## OUTPUT

Prints to stdout:

`<reps> commit(s) completed over <total time> minute(s).`

## PROBLEMS

If you encounter a bug, please report it using this form: https://docs.google.com/forms/d/e/1FAIpQLSegE7gBZKNqU2bAiJKjwGBUp5FiCO5DJm9PR6GtMoY_dBQBtg/viewform?usp=sf_link

### ERRORS

### UNEXPECTED/UNWANTED BEHAVIORS

### ARCHITECTURE

+ All variables are prepended with `gm_` to prevent overriding other global variables, but this is still possible since most of the script is not contained within a function.
+ git commands do not behave properly when inclosed in a function, script is bloated as a result.

## AUTHOR

Anthony Umemoto 2022, with debugging assistance from Omar Ahmadyar.